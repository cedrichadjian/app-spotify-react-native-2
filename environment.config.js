// Examples to be ran:
// 1) npm run locale:youtube fr dark && npm run android (to run the app in youtube, dark, french, android)
// 2) npm run locale:spotify en light && npm run ios (to run the app in spotify, light, english, ios)

const fs = require("fs");
const copydir = require("copy-dir");

if (process.argv.length < 4) {
  console.log(process.argv);
  console.error("Missing arguments!");
  process.exit(1);
}

const LOCALE = process.argv[2];
const LANG = process.argv[3];
const THEME = process.argv[4];

const wording = require(`./src/locales/${LOCALE}/i18n/${LANG}.json`); // in which language the wording should be
fs.writeFileSync("./src/locales/main/wording.json", JSON.stringify(wording, undefined, 2));

const theme = require(`./src/locales/${LOCALE}/theme/${THEME}.json`); // in which theme the app will be (dark/light)
fs.writeFileSync("./src/locales/main/colors.json", JSON.stringify(theme, undefined, 2));

const assets_dir = `./src/locales/${LOCALE}/assets`; // in which assets the app will be used
copydir(assets_dir, "./src/locales/main/assets", (err) =>
  err ? console.log(err) : console.log("Assets written successfully!")
);
