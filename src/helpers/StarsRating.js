import React, { Component } from "react";
import { View, Image } from "react-native";
import stars_rating_styles from "../styles/stars_rating_styles";

export default class StarsRating extends Component {
  render() {
    const { popularity } = this.props;
    let stars = "";

    if (popularity < 20) stars = 1;
    if (popularity >= 20 && popularity < 40) stars = 2;
    if (popularity >= 40 && popularity < 60) stars = 3;
    if (popularity >= 60 && popularity < 80) stars = 4;
    if (popularity >= 80 && popularity <= 100) stars = 5;

    let StarsRatingView = [];

    for (let i = 0; i < stars; i++) {
      StarsRatingView.push(
        <Image
          source={require("../locales/main/assets/star.png")}
          style={stars_rating_styles.star_rating}
          key={Math.floor(Math.random() * 1000000)}
        />
      );
    }

    return <View style={stars_rating_styles.stars_rating}>{StarsRatingView}</View>;
  }
}
