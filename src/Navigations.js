import React, { Component } from "react";
import { LinearGradient } from "expo-linear-gradient";
import { NavigationContainer, DefaultTheme } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { observer } from "mobx-react";
import store from "./store/store";

import Login from "./components/Login";
import Search from "./components/Search";
import Albums from "./components/Albums";
import Logout from "./components/Logout";
import Authentication from "./services/Authentication";
import theme from "./locales/main/colors.json";
import wording from "./locales/main/wording.json";

const Stack = createStackNavigator();
const NavigationTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: theme.firstColor.hex,
  },
};

@observer
export default class Navigations extends Component {
  async componentDidMount() {
    const tokenExpirationTime = store.tokens.expiration_time;
    if (tokenExpirationTime && new Date().getTime() > tokenExpirationTime) await Authentication.refreshtokens();
  }

  render() {
    return (
      <NavigationContainer theme={NavigationTheme}>
        <Stack.Navigator
          initialRouteName="Login"
          screenOptions={{
            headerStyle: {
              backgroundColor: theme.firstColor.hex,
            },
            headerTitleStyle: {
              color: "#fff",
              alignSelf: "center",
            },
            headerBackground: () => (
              <LinearGradient
                colors={["#a13388", "#10356c"]}
                style={{ flex: 1 }}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
              />
            ),
            headerTintColor: "#fff",
          }}
        >
          {!store.tokens.access_token && (
            <Stack.Screen
              name="Login"
              component={Login}
              options={{
                title: wording.welcome,
              }}
            />
          )}

          {store.tokens.access_token && (
            <Stack.Screen
              name="Search"
              component={Search}
              options={{
                headerRight: () => <Logout />,
              }}
            />
          )}

          {store.tokens.access_token && (
            <Stack.Screen
              name="Albums"
              component={Albums}
              options={({ route }) => ({
                title: route.params.name,
                headerRight: () => <Logout />,
              })}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
