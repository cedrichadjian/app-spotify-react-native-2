import React, { Component } from "react";
import { View, Text, TouchableOpacity, ImageBackground } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import theme from "../locales/main/colors.json";
import login_styles from "../styles/login_styles";
import wording from "../locales/main/wording.json";

import Authentication from "../services/Authentication";
import store from "../store/store";
import { Observer } from "mobx-react";
import { LinearGradient } from "expo-linear-gradient";

@Observer
export default class Login extends Component {
  async handleOnPress() {
    const { access_token, refresh_token, expiration_time } = await Authentication.getTokens();
    Object.assign(store.tokens, { access_token, refresh_token, expiration_time });
  }

  render() {
    return (
      <View style={login_styles.container}>
        <View style={login_styles.login}>
          <TouchableOpacity onPress={this.handleOnPress}>
            <Text style={login_styles.login_button}>{wording.login}</Text>
          </TouchableOpacity>
          <FontAwesome style={login_styles.login_fa} name={"spotify"} size={40} color={"#1DB954"} />
        </View>

        <ImageBackground source={require("../locales/main/assets/neon-girl.png")} style={login_styles.bg_img}>
          <LinearGradient
            locations={[1, 0]}
            colors={["rgba(0,0,0,0)", theme.firstColor.rgba]}
            style={{
              position: "absolute",
              width: "100%",
              height: "100%",
            }}
          />
        </ImageBackground>
      </View>
    );
  }
}
