import React, { Component } from "react";
import albums_styles from "../styles/albums_styles";

import { FlatList } from "react-native";

import { observer } from "mobx-react";
import store from "../store/store";
import AlbumsResults from "../services/AlbumsResults";
import Album from "../services/SingleAlbum";

@observer
class Albums extends Component {
  componentDidMount() {
    const { id } = this.props.route.params;
    AlbumsResults.getAlbumsResults(id);
  }

  render() {
    return (
      <FlatList
        contentContainerStyle={albums_styles.albums_container}
        data={store.albumsStore.albums}
        renderItem={({ item }) => (
          <Album
            id={item.id}
            name={item.name}
            image={
              item.images.length ? { uri: item.images[0].url } : require("../locales/main/assets/placeholder-album.png")
            }
            url={item.external_urls.spotify}
            artists={item.artists}
            release_date={item.release_date}
            total_tracks={item.total_tracks}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
        onEndReached={AlbumsResults.getMoreAlbumsResults}
      />
    );
  }
}

export default Albums;
