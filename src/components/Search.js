import React, { Component } from "react";
import theme from "../locales/main/colors.json";
import search_styles from "../styles/search_styles";
import wording from "../locales/main/wording.json";

import { View, FlatList, Text, ActivityIndicator } from "react-native";
import { Input, Icon } from "react-native-elements";

import { observer } from "mobx-react";
import store from "../store/store";
import "mobx-react/batchingForReactNative";

import _ from "lodash";

import SearchResults from "../services/SearchResults";

import Artist from "../services/SingleArtist";

@observer
class Search extends Component {
  constructor(props) {
    super(props);
    this.delayedSearch = _.debounce(SearchResults.getSearchResults, 1000);
  }

  searchInput = React.createRef();

  render() {
    return (
      <View>
        <View style={search_styles.searchWrapper}>
          <Input
            autoFocus
            containerStyle={search_styles.search}
            placeholder={wording.searchPlaceholder}
            onChangeText={this.delayedSearch}
            leftIcon={{ type: "font-awesome", name: "search", color: theme.firstColor.hex }}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            placeholderTextColor={theme.firstColor.hex}
            inputStyle={{
              color: theme.firstColor.hex,
            }}
            ref={this.searchInput}
            rightIcon={
              <Icon
                containerStyle={{
                  marginRight: 10,
                  display: !store.searchStore.search ? "none" : "flex",
                }}
                name={"ios-close"}
                type={"ionicon"}
                color="#7384B4"
                size={40}
                onPress={() => {
                  this.searchInput.current.clear();
                  store.searchStore.search = null;
                }}
              />
            }
          />
        </View>
        <View>
          {store.searchStore.artists.length && store.searchStore.search ? (
            <FlatList
              data={store.searchStore.artists}
              renderItem={({ item }) => (
                <Artist
                  id={item.id}
                  name={item.name}
                  image={
                    item.images.length
                      ? { uri: item.images[0].url }
                      : require("../locales/main/assets/placeholder-artist.png")
                  }
                  followers={item.followers.total}
                  popularity={item.popularity}
                  navigation={this.props.navigation}
                />
              )}
              keyExtractor={(item, index) => index.toString()}
              style={search_styles.artists}
              onEndReached={SearchResults.getMoreSearchResults}
            />
          ) : (
            []
          )}

          {!store.searchStore.artists.length && store.searchStore.search && !store.searchStore.fetching ? (
            <View style={search_styles.not_found}>
              <Text style={search_styles.not_found_text}>{wording.noResults}</Text>
              <Text style={search_styles.not_found_search}>"{store.searchStore.search}"</Text>
            </View>
          ) : (
            []
          )}

          {store.searchStore.fetching && (
            <ActivityIndicator size="large" color={theme.secondColor} style={search_styles.fetching} />
          )}
        </View>
      </View>
    );
  }
}

export default Search;
