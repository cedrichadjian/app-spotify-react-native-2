import React, { Component } from "react";
import logout_styles from "../styles/logout_styles";
import wording from "../locales/main/wording.json";

import { Text, TouchableOpacity } from "react-native";
import { Observer } from "mobx-react";
import store from "../store/store";

@Observer
class Logout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleLogout = async () => {
    Object.assign(store.tokens, {
      access_token: null,
      refresh_token: null,
      expiration_time: null,
    });
    Object.assign(store.searchStore, {
      search: null,
      artists: [],
      offset: 0,
      limit: 20,
      fetching: false,
      page: 1,
    });
    Object.assign(store.albumsStore, {
      id: "",
      name: "",
      albums: [],
      offset: 0,
      limit: 20,
      page: 1,
    });
  };

  render() {
    return (
      <TouchableOpacity onPress={this.handleLogout}>
        <Text style={logout_styles.logout}>{wording.logout}</Text>
      </TouchableOpacity>
    );
  }
}

export default Logout;
