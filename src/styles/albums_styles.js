import theme from "../locales/main/colors.json";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  albums_container: {
    marginLeft: 20,
  },
  album: {
    flexDirection: "row",
    marginTop: 25,
    alignItems: "center",
  },
  album_img: {
    width: 75,
    height: 75,
  },
  album_details: {
    marginLeft: 10,
  },
  album_name: {
    color: theme.secondColor,
  },
  album_artists: {
    color: theme.secondColor,
  },
  album_artist: {
    color: theme.secondColor,
  },
  album_releasedate: {
    color: theme.secondColor,
  },
  album_tracks: {
    color: theme.secondColor,
  },
});
