import { StyleSheet } from "react-native";

export default StyleSheet.create({
  stars_rating: {
    flexDirection: "row",
  },
  star_rating: {
    width: 15,
    height: 15,
  },
});
