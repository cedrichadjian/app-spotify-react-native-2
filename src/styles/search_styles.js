import theme from "../locales/main/colors.json";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  searchWrapper: {
    marginTop: 40,
    alignItems: "center",
  },
  search: {
    backgroundColor: theme.secondColor,
    paddingLeft: 20,
    borderRadius: 10,
    borderColor: theme.secondColor,
    width: 300,
    height: 50,
  },
  artists: {
    marginTop: 20,
    marginBottom: "20%",
  },
  artist: {
    flexDirection: "row",
    padding: 5,
    marginVertical: 8,
    marginHorizontal: 16,
    alignItems: "center",
  },
  artist_image: {
    width: 50,
    height: 50,
    borderRadius: 400 / 2,
  },
  artist_details: {
    marginLeft: 10,
  },
  artist_name: {
    color: theme.secondColor,
  },
  artist_followers: {
    color: theme.secondColor,
  },
  fetching: {
    marginTop: 20,
  },
  not_found: {
    alignItems: "center",
    marginTop: "20%",
  },
  not_found_text: {
    color: theme.secondColor,
    fontSize: 20,
  },
  not_found_search: {
    color: theme.secondColor,
    fontSize: 18,
  },
});
