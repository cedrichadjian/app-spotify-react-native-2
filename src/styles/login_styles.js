import theme from "../locales/main/colors.json";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  bg_img: {
    width: "100%",
    height: 350,
    position: "absolute",
    bottom: 0,
  },
  login: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    marginTop: 50,
    borderColor: theme.secondColor,
    borderWidth: 1,
    borderStyle: "solid",
    borderRadius: 5,
    paddingTop: 15,
    paddingBottom: 15,
  },
  login_button: {
    color: theme.secondColor,
    fontSize: 20,
    paddingRight: 100,
    paddingLeft: 100,
  },
  login_fa: {
    position: "absolute",
    left: 200,
  },
});
