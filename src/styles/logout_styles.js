import theme from "../locales/main/colors.json";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  logout: {
    color: theme.exceptionWhite,
    fontSize: 17,
    marginRight: 10,
    fontWeight: "600",
  },
});
