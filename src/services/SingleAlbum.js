import React, { Component } from "react";
import { TouchableOpacity, Text, Image, View, Linking } from "react-native";
import albums_styles from "../styles/albums_styles";
import errorHandler from "../helpers/errorHandler";

const onSelect = (url) => {
  Linking.openURL(url).catch((err) => errorHandler(err));
};

export default function Album({ id, name, image, url, artists, release_date, total_tracks }) {
  let artistNames = [];
  artists.map((artist) => artistNames.push(artist.name));
  return (
    <TouchableOpacity style={albums_styles.album} onPress={() => onSelect(url)}>
      <Image source={image} style={albums_styles.album_img} />
      <View style={albums_styles.album_details}>
        <Text style={albums_styles.album_name}>{name}</Text>
        <View style={albums_styles.album_artists}>
          <Text style={albums_styles.album_artist}>{artistNames.join(", ")}</Text>
        </View>
        <Text style={albums_styles.album_releasedate}>{release_date}</Text>
        <Text style={albums_styles.album_tracks}>{total_tracks} tracks</Text>
      </View>
    </TouchableOpacity>
  );
}
