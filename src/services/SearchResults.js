import { observer } from "mobx-react";
import store from "../store/store";

import axios from "axios";
import errorHandler from "../helpers/errorHandler";

@observer
export default class SearchResults {
  static getSpotifyURL = () => {
    return {
      url: `/search?access_token=${store.tokens.access_token}&q=${store.searchStore.search}&offset=${store.searchStore.offset}&limit=${store.searchStore.limit}&type=artist`,
      method: "get",
      baseURL: "https://api.spotify.com/v1/",
    };
  };

  static getSearchResults = (search) => {
    store.searchStore.search = search;
    store.searchStore.offset = 0;
    store.searchStore.fetching = true;
    store.searchStore.page = 1;
    if (store.searchStore.search) {
      axios
        .request(this.getSpotifyURL())
        .then((response) => {
          store.searchStore.artists = response.data.artists.items;
          store.searchStore.fetching = false;
        })
        .catch((err) => {
          errorHandler(err);
        });
    } else {
      store.searchStore.artists = [];
      store.searchStore.fetching = false;
    }
  };

  static getMoreSearchResults = () => {
    let page = store.searchStore.page;
    const offset = (page - 1) * store.searchStore.limit + 1;
    store.searchStore.offset = offset;
    axios
      .request(this.getSpotifyURL())
      .then((response) => {
        store.searchStore.artists = store.searchStore.artists.concat(response.data.artists.items);
        store.searchStore.page = store.searchStore.page + 1;
      })
      .catch((err) => {
        errorHandler(err);
      });
  };
}
