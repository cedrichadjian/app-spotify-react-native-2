import axios from "axios";
import errorHandler from "../helpers/errorHandler";
import { observer } from "mobx-react";
import store from "../store/store";

@observer
export default class AlbumsResults {
  static getSpotifyAlbumsURL = (id) => {
    return {
      url: `/artists/${id}/albums?access_token=${store.tokens.access_token}&offset=${store.albumsStore.offset}&limit=${store.albumsStore.limit}`,
      method: "get",
      baseURL: "https://api.spotify.com/v1/",
    };
  };

  static getAlbumsResults = (id) => {
    store.albumsStore.id = id;
    axios
      .get(`https://api.spotify.com/v1/artists/${id}?access_token=${store.tokens.access_token}`)
      .then((response) => {
        store.albumsStore.name = response.data.name;
        axios
          .request(this.getSpotifyAlbumsURL(id))
          .then((response) => {
            store.albumsStore.albums = response.data.items;
          })
          .catch((error) => {
            errorHandler(error);
          });
      })
      .catch((error) => {
        errorHandler(error);
      });
  };
  static getMoreAlbumsResults = () => {
    let page = store.albumsStore.page;
    const offset = (page - 1) * store.albumsStore.limit + 1;
    store.albumsStore.offset = offset;
    axios
      .request(this.getSpotifyAlbumsURL(store.albumsStore.id))
      .then((response) => {
        const albums_list = response.data.items;
        if (albums_list.length) {
          store.albumsStore.albums = store.albumsStore.albums.concat(albums_list);
          store.albumsStore.page = page + 1;
        }
      })
      .catch((error) => {
        errorHandler(error);
      });
  };
}
