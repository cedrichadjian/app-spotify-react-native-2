import * as AuthSession from "expo-auth-session";
import { encode as btoa } from "base-64";
import errorHandler from "../helpers/errorHandler";

const credentials = {
  clientId: "d64aeac9b75e42d793e2ced9277d1ec1",
  clientSecret: "cdec307175894f788463f633dcd1581b",
  redirectUri: "https://auth.expo.io/@cedrichadjian/react-native-spotify",
};

const getAuthorizationCode = async () => {
  try {
    const redirectUrl = AuthSession.getRedirectUrl();
    const result = await AuthSession.startAsync({
      authUrl:
        "https://accounts.spotify.com/authorize" +
        "?response_type=code" +
        "&client_id=" +
        credentials.clientId +
        "&redirect_uri=" +
        encodeURIComponent(redirectUrl),
    });
    if (result.type === "success") return result.params.code;
  } catch (err) {
    errorHandler(err);
  }
};

const getResponse = async (type) => {
  try {
    const authorizationCode = await getAuthorizationCode();
    const credsB64 = btoa(`${credentials.clientId}:${credentials.clientSecret}`);
    let body =
      type === "refresh"
        ? `grant_type=refresh_token&refresh_token=${refreshToken}`
        : `grant_type=authorization_code&code=${authorizationCode}&redirect_uri=${credentials.redirectUri}`;
    const response = await fetch("https://accounts.spotify.com/api/token", {
      method: "POST",
      headers: {
        Authorization: `Basic ${credsB64}`,
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body,
    });
    return response.json();
  } catch (err) {
    errorHandler(err);
  }
};

const getTokens = async () => {
  try {
    const responseJson = await getResponse();
    const { access_token, refresh_token, expires_in } = responseJson;
    const expiration_time = new Date().getTime() + expires_in * 1000;
    return {
      access_token,
      refresh_token,
      expiration_time,
    };
  } catch (err) {
    errorHandler(err);
  }
};

const refreshtokens = async () => {
  try {
    const responseJson = await getResponse("refresh");
    if (responseJson.error) {
      await getTokens();
    } else {
      const { access_token: newAccessToken, refresh_token: newRefreshToken, expires_in: expiresIn } = responseJson;
      const expirationTime = new Date().getTime() + expiresIn * 1000;
      if (newRefreshToken) {
        return {
          access_token: newAccessToken,
          refresh_token: newRefreshToken,
          expiration_time: expirationTime,
        };
      }
    }
  } catch (err) {
    errorHandler(err);
  }
};

export default {
  getTokens,
  refreshtokens,
};
