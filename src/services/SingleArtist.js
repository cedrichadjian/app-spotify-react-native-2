import React, { Component } from "react";
import { TouchableOpacity, Text, Image, View } from "react-native";
import search_styles from "../styles/search_styles";
import StarsRating from "../helpers/StarsRating";

const onSelect = (id, name, navigation) => {
  navigation.navigate("Albums", {
    id,
    name,
  });
};

export default function Artist({ id, name, image, followers, popularity, navigation }) {
  return (
    <TouchableOpacity style={search_styles.artist} onPress={() => onSelect(id, name, navigation)}>
      <Image source={image} style={search_styles.artist_image} />
      <View style={search_styles.artist_details}>
        <Text style={search_styles.artist_name}>{name}</Text>
        <Text style={search_styles.artist_followers}>{followers.toLocaleString()} followers</Text>
        <StarsRating popularity={popularity} />
      </View>
    </TouchableOpacity>
  );
}
