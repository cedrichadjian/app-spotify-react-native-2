import { observable } from "mobx";

class GlobalStore {
  @observable tokens = {
    access_token: null,
    refresh_token: null,
    expiration_time: null,
  };
  @observable searchStore = {
    search: null,
    artists: [],
    offset: 0,
    limit: 20,
    fetching: false,
    page: 1,
  };
  @observable albumsStore = {
    id: "",
    name: "",
    albums: [],
    offset: 0,
    limit: 20,
    page: 1,
  };
}

const store = new GlobalStore();
export default store;
